﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            note Note1;
            Note1 = new note();
            note Note2;
            Note2 = new note("Pozz", "Josip Uhlik", 1);
            note Note3;
            Note3 = new note("Josip Uhlik", 2);
            Console.Write(Note2.ToString());
            ToDo_List Lista_za_napraviti = new ToDo_List();
            for (int i = 0; i < 3; i++)
            {
                Console.Write("Unesi Biljesku: ");
                string Biljeska = Console.ReadLine();
                Console.Write("Unesi Autora: ");
                string Autor = Console.ReadLine();
                Console.Write("Unesi prioritet: ");
                int Prioritet = Convert.ToInt32(Console.ReadLine());
                Lista_za_napraviti.addTolist(Biljeska, Autor, Prioritet);
            }
            Console.Write(Lista_za_napraviti.ToString());
            Lista_za_napraviti.removeWithSamePriority(2);
            Console.Write(Lista_za_napraviti.ToString());
        }
    }
}
