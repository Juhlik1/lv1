﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class note_with_timeStamp:note
    {
        private DateTime mTime;

        public note_with_timeStamp(string Txt, string Note_autor, int priority) : base(Txt, Note_autor, priority)
        {
            mTime = DateTime.Now ;
        }
        public note_with_timeStamp(string Txt, string Note_autor, int priority, DateTime Time):base(Txt,Note_autor,priority)
        {
            mTime = Time;
        }
        public note_with_timeStamp(note_with_timeStamp note1) : base(note1.MTxt,note1.MNote_Autor,note1.Mpriority)
        {
            this.mTime = note1.mTime;
        }
        public DateTime Mtime
        {
            set { this.mTime = value; }
            private get {  return this.mTime; }
        }
        public override string ToString()
        {
            return "Tekst: " + this.MTxt + " Autor: " + this.MNote_Autor + " Prioritet: " + this.Mpriority + " Vrijeme: " + Mtime + "\n";
        }
    }
}
