﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class note
    {
        private string mTxt;
        private string mNote_autor;
        private int mPriority;
        public note()
        {
            mTxt = "";
            mNote_autor = "";
            mPriority = 0;
        }
        public note(string Txt, string Note_autor, int priority)
        {
            mTxt = Txt;
            mNote_autor = Note_autor;
            mPriority = priority;
        }
        public note(string Note_autor, int priority)
        {
            mTxt = "";
            mNote_autor = Note_autor;
            mPriority = priority;
        }
        string getTxt() { return mTxt; }
        string getAutor() { return mNote_autor; }
        int getPriority() { return mPriority; }
        void setTxt(string txt) { mTxt=txt; }
        void setPriority(int Priority) { mPriority=Priority; }

        public string MTxt
        {
            get { return this.mTxt; }
            set { this.mTxt = value; }
        }
        public int Mpriority
        {
            get { return this.mPriority; }
            set { this.mPriority = value; }
        }
        public string MNote_Autor
        {
            get { return this.mNote_autor; }
            set { this.mNote_autor = value; }
        }
        public override string ToString()
        {
            return "Tekst: " + this.MTxt + " Autor: " + this.MNote_Autor + " Prioritet: " + this.Mpriority;
        }
    }

}