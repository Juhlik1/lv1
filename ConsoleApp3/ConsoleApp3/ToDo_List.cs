﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class ToDo_List
    {
        private List<note_with_timeStamp> lista;
        public ToDo_List()
        {
            lista = new List<note_with_timeStamp>();
        }
        public void addTolist(string txt, string autor, int prioritet) {
            lista.Add(new note_with_timeStamp(txt, autor, prioritet) {});     
                }
        public void removeFromlist(int index) {
            lista.RemoveAt(index);
        }
        public string GetNote(int priority)
        {
            List<note_with_timeStamp> NoteWithLookedPriority = new List<note_with_timeStamp>();
            foreach(note_with_timeStamp aNote in lista)
            {
                if (priority == aNote.Mpriority)
                {
                    NoteWithLookedPriority.Add(new note_with_timeStamp(aNote));
                }
            }
            return string.Join(",", NoteWithLookedPriority);
        }
        public void removeWithSamePriority(int priority)
        {
            lista.Remove(new note_with_timeStamp("","", priority) {});
        }
        public override string ToString()
        {
            return string.Join("" , lista);
        }
    }
}
